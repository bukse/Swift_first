import UIKit

// 1

class Shape {
    func calculateArea() -> Double {
        fatalError("Not implemented")
    }

    func calculatePerimeter() -> Double {
        fatalError("Not implemented")
    }
}

final class Rectangle: Shape {
    private let height: Double
    private let width: Double

    init(height: Double, width: Double) {
        self.height = height
        self.width = width
    }

    override func calculateArea() -> Double {
        return self.height * self.width
    }

    override func calculatePerimeter() -> Double {
        return (self.height + self.width) * 2
    }
}

final class Circle: Shape {
    private let radius: Double

    init(radius: Double) {
        self.radius = radius
    }

    override func calculateArea() -> Double {
        return Double.pi * self.radius * self.radius
    }

    override func calculatePerimeter() -> Double {
        return Double.pi * self.radius * 2
    }
}

final class Square: Shape {
    private let side: Double

    init(side: Double) {
        self.side = side
    }

    override func calculateArea() -> Double {
        return self.side * self.side
    }

    override func calculatePerimeter() -> Double {
        return self.side * 4
    }
}

var area: Double = 0.0
var perimeter: Double = 0.0

let shapes: [Shape] = [Rectangle(height: 1, width: 2), Circle(radius: 3), Square(side: 4)]

for i in shapes {
    area = area + i.calculateArea()
    perimeter = perimeter + i.calculatePerimeter()
}

print("Sum of all areas: \(area)")
print("sum of all perimeters: \(perimeter)")
