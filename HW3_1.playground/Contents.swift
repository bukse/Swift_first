import UIKit

func makeBuffer() -> (String) -> Void {
    var text = ""
    
    func concatText (value: String) {
        !value.isEmpty ? text += value : print(text)
    }
    
    return concatText
}

var buffer = makeBuffer()

buffer("Замыкание")
buffer(" использовать")
buffer(" нужно!")
buffer("")
