// Задача 1

let milkmanPhrase: String = "Молоко - это полезно"

print(milkmanPhrase)

// Задача 2

// let milkPrice: Int = 3

// Задача 3

/*
 milkPrice = 4.20 сделать не получится по двум причинам:
 из-за let и типа Int. Поэтому закоментим константу на строке 9
 и создадим новую переменную milkPrice с типом Double
*/

var milkPrice: Double = 3.0
milkPrice = 4.20

// Задача 4

var milkBottleCount: Int? = 20
var profit: Double = 0.0

profit = Double(milkBottleCount!) * milkPrice
print(profit)

// Доп задание к задаче 4
/*
if let bottleCount = milkBottleCount {
    profit = Double(bottleCount) * milkPrice
    print(profit)
} else {
    print("Бутылок не привезли")
}
*/
 
// Задача 5

var employeesList: [String]
employeesList = ["Иван", "Пётр", "Геннадий", "Марфа", "Андрей"]

// Задача 6

var isEveryoneWorkHard: Bool = false
let workingHours: Int = 40

if workingHours >= 40 {
    isEveryoneWorkHard = true
} else {
    isEveryoneWorkHard = false
}

print(isEveryoneWorkHard)

// Ниже стало интерсно и решил поиграться, надеюсь такое не наказывается

// isEveryoneWorkHard ? print("Работал хорошо") : print("Работал плохо")

/*
 
for employee in employeesList {
    Int.random(in: 35...45) >= workingHours ? print("\(employee) работал хорошо") : print("\(employee) работал плохо")
}
 
*/
