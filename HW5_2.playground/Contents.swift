import UIKit

/*
func findIndex(ofString valueToFind: String, in array: [String]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]

if let foundIndex = findIndex(ofString: "лама", in: arrayOfString) {
    print("Индекс ламы: \(foundIndex)")
}
*/

func findIndexWithGenerics<T: Equatable>(of valueToFind: T, in array:[T]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

let arrayOfStrings = ["кот", "пес", "лама", "попугай", "черепаха"]
if let index = findIndexWithGenerics(of: "кот", in: arrayOfStrings) {
    print("Индекс кота: ", index)
}

let arrayOfDoubles = [1.0, 1.1, 1.2, 1.3, 1.4]
if let index = findIndexWithGenerics(of: 1.1, in: arrayOfDoubles) {
    print("Индекс числа 1.1: ", index)
}

let arrayOfInts = [1, 2, 3, 4, 5]
if let index = findIndexWithGenerics(of: 3, in: arrayOfInts) {
    print("Индекс числа 3: ", index)
}
