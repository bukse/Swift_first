// Немного расширил решение, добавив имя к фимилиям и сделав через структуру. Хотелось попробовать так.
// Как я понимаю, такое только приветствуется.

struct Student {
    var firstName: String
    var lastName: String
}

class Students {
    private var students: [Student] = []
    
    private func sortList() {
        students.sort { $0.lastName.first! < $1.lastName.first! }
    }
    
    func addStudent(student: Student) {
        students.append(student)
        sortList()
    }
    
    func getList() -> [Student] {
        return students
    }
}

let firstClass = Students()
firstClass.addStudent(student: Student(firstName: "Даша", lastName: "Акулова"))
firstClass.addStudent(student: Student(firstName: "Александр", lastName: "Валерьянов"))
firstClass.addStudent(student: Student(firstName: "Кирилл", lastName: "Бочкин"))
firstClass.addStudent(student: Student(firstName: "Маша", lastName: "Бабочкина"))
firstClass.addStudent(student: Student(firstName: "Даниил", lastName: "Арбузов"))
firstClass.addStudent(student: Student(firstName: "Кристина", lastName: "Васильева"))

let firstClassStudents = firstClass.getList()


for firstClassStudent in firstClassStudents {
    // Знаю про раскрытие через !, что это не есть хорошо. Но хотелось добавить немного красоты.
    // Также знаю про то, что не все фамилии склоняемы. Но хотелось добавть немного красоты :peka:
    if (firstClassStudent.lastName.last! == "а") {
        print("Студентка \(firstClassStudent.lastName) \(firstClassStudent.firstName)")
    } else {
        print("Студент \(firstClassStudent.lastName) \(firstClassStudent.firstName)")
    }
}
