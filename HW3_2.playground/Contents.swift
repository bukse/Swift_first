func checkPrimeNumber(_ nubmerForCheck: Int) -> Bool {
    guard nubmerForCheck > 1 else { return false }
    guard nubmerForCheck != 2 else { return true }
    for n in 2...nubmerForCheck - 1 {
        if nubmerForCheck % n == 0 {
            return false
        }
    }
    
    return true

}

checkPrimeNumber(2) // true
checkPrimeNumber(3) // true
checkPrimeNumber(4) // false
checkPrimeNumber(7) // true
checkPrimeNumber(8) // false
checkPrimeNumber(13) // true
