import UIKit

struct Device {
    let screenSize: CGSize
    let screenDiagonal: Double
    let scaleFactor: Int
    
    func physicalSize() -> CGSize {
        return CGSize(
            width: screenSize.width * CGFloat(scaleFactor),
            height: screenSize.height * CGFloat(scaleFactor)
        )
    }
    
    static let iPhone14Pro = Device(screenSize: CGSize(width: 393, height: 852), screenDiagonal: 6.1, scaleFactor: 3)
    static let iPhoneXR = Device(screenSize: CGSize(width: 414, height: 896), screenDiagonal: 6.06, scaleFactor: 2)
    static let iPadPro = Device(screenSize: CGSize(width: 834, height: 1194), screenDiagonal: 11, scaleFactor: 2)
}

Device.iPhone14Pro.physicalSize() // w 1179 h 2556
Device.iPhoneXR.physicalSize() // w 828 h 1792
Device.iPadPro.physicalSize() // w 1668 h 2388

