struct Candidate {
    enum Grade {
        case junior
        case middle
        case senior
    }
     
    /// Грейд (уровень) кандидата
    let grade: Grade
    /// Требуемая зарплата
    let requiredSalary: Int
    /// Полное имя кандидата
    let fullName: String
}

protocol Filterable {
    func filter(candidates: [Candidate]) -> [Candidate]
}

struct GradeFilter: Filterable {
    let grade: Candidate.Grade
    
    func filter(candidates: [Candidate]) -> [Candidate] {
        return candidates.filter{ $0.grade == grade }
    }
}

struct NameFilter: Filterable {
    let name: String
    
    func filter(candidates: [Candidate]) -> [Candidate] {
        return candidates.filter{ $0.fullName.contains(name) }
    }
}

struct SalaryFilter: Filterable {
    let salary: Int
    
    func filter(candidates: [Candidate]) -> [Candidate] {
        return candidates.filter{ $0.requiredSalary <= salary }
    }
}

// Часть задачи 2. Убрал сюда, чтобы все было друг за дружкой
struct MinGragedeFilter: Filterable {
    let minGrade: Candidate.Grade
    
    func filter(candidates: [Candidate]) -> [Candidate] {
        return candidates.filter{ $0.grade >= minGrade}
    }
}

let candidates: [Candidate] = [
    Candidate(grade: .junior, requiredSalary: 100500, fullName: "Иванов Иван Джунович"),
    Candidate(grade: .junior, requiredSalary: 150000, fullName: "Джун Илья Иванович"),
    Candidate(grade: .middle, requiredSalary: 300000, fullName: "Мидл Плюс Плюсович"),
    Candidate(grade: .senior, requiredSalary: 400000, fullName: "Синьера Помидора Татьяновна"),
    Candidate(grade: .senior, requiredSalary: 500000, fullName: "Помидор Сергей Олегович")
]

let filteredByGrade = GradeFilter(grade: .middle).filter(candidates: candidates)
let filteredBySalary = SalaryFilter(salary: 150000).filter(candidates: candidates)
let filteredByName = NameFilter(name: "Иван").filter(candidates: candidates)

//print(filteredByGrade)
//print(filteredBySalary)
//print(filteredByName)

// Решил добавить красоты
print("По грейдам нам подходят: \(filteredByGrade.map { $0.fullName })")
print("По зарплате нам подходят: \(filteredBySalary.map { $0.fullName })")

// Задача 2

extension Candidate.Grade {
/*  Мысли вслух, не стал удалять. Выглядит не очень, если будет больше гредов. Допустим, если добавить jun+, mid+, итд
    static func >= (a: Candidate.Grade, b: Candidate.Grade) -> Bool {
        switch (a, b) {
        case (.junior, .junior): return true
        case (.junior, .middle): return true
        case (.junior, .senior): return true
        case (.middle, .middle): return true
        case (.middle, .senior): return true
        case (.senior, .senior): return true
        default: return false
        }
    }
*/
    
    var rowNumer: Int {
        switch self {
        case .junior: return 1
        case .middle: return 2
        case .senior: return 3
        }
    }
    
    static func >= (a: Candidate.Grade, b: Candidate.Grade) -> Bool {
            return a.rowNumer >= b.rowNumer
    }
}

let filteredByMinGrade = MinGragedeFilter(minGrade: .middle).filter(candidates: candidates)

print("По минимальному грейду нам подходят: \(filteredByMinGrade.map { $0.fullName })")
